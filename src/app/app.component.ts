import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { MenuService } from 'src/services/menu.service';

@Component({
  selector: 'mf-menu',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'mf-menu';

  navType: string;
  themeLayout: string;
  layoutType: string;
  verticalPlacement: string;
  verticalLayout: string;
  deviceType: string;
  verticalNavType: string;
  verticalEffect: string;
  vNavigationView: string;

  headerFixedMargin: string;
  pcodedHeaderPosition: string;
  pcodedSidebarPosition: string;
  navBarTheme: string;
  activeItemTheme: string;

  menuTitleTheme: string;
  itemBorder: boolean;
  itemBorderStyle: string;
  subItemBorder: boolean;
  dropDownIcon: string;
  subItemIcon: string;

  toggleOn: boolean;
  windowWidth: number;

  showMenu: boolean;

  menus = [];

  constructor(public _menuService: MenuService) {
    this.navType = 'st2';
    this.themeLayout = 'vertical';
    this.verticalPlacement = 'left';
    this.verticalLayout = 'wide';
    this.deviceType = 'desktop';
    this.verticalNavType = 'expanded';
    this.verticalEffect = 'shrink';
    this.vNavigationView = 'view1';

    this.menuTitleTheme = 'theme5';
    this.itemBorder = true;
    this.itemBorderStyle = 'none';
    this.subItemBorder = true;
    this.dropDownIcon = 'style1';
    this.subItemIcon = 'style6';

    this.navBarTheme = 'themelight1';
    this.activeItemTheme = 'theme4';
    this.headerFixedMargin = '0px';
    
    this.pcodedHeaderPosition = 'fixed';
    this.pcodedSidebarPosition = 'fixed';

    this.toggleOn = true;
    this.windowWidth = window.innerWidth;

    this.showMenu = true;
    /*(window as any).subscribe('menu-select', (msg: any) => {
      console.log(msg);
      //this.toggleOpened();
      this.cambiarShow();
    });*/
  }

  ngOnInit(): void {
    this.hideShowMenu();
    setTimeout(() => {
      console.log("[MENUS] ==> ", this._menuService.getAll());
      this.menus = this._menuService.getAll();
    }, 5000);
  }

  hideShowMenu() {
    let eventSubs = fromEvent(window, 'hideShowMenu');
    eventSubs.subscribe(
      resp => {
        this.showMenu = resp["detail"]["answer"];
      }
    );
  }

  onResize($event) {

  }

  onClickedOutside($event) {
    console.log("click onClickedOutside");
  }
  
  toggleOpened() {
    if (this.windowWidth < 768) {
      this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
    }
    this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
  }

  cambiarShow() {
    console.log("llama a la funcion show");
  }

}
