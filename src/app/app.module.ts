import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { MenuService } from 'src/services/menu.service';
import { HttpClientModule } from '@angular/common/http';
import { AccordionAnchorDirective, AccordionDirective, AccordionLinkDirective } from './shared/accordion';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  declarations: [
    AppComponent,
    EmptyRouteComponent,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    PerfectScrollbarModule
  ],
  providers: [MenuService],
  bootstrap: [AppComponent]
})
export class AppModule { }
