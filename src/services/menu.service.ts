import { Injectable } from "@angular/core";
import { ChildrenItems, MainMenuItems, MenuModel, SeguradMenu } from './menu.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { map, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class MenuService {

    menus: MenuModel[];

    constructor(private _http : HttpClient) {
        let array = new Array<any>();
        this.getMenus().subscribe(
            resp => {
                array = resp;
                this.menus = this.createNuevoMenu(array);
                console.log("Menus", this.menus);
            }
        );
    }

    getAll(): MenuModel[] {
        return this.menus;
    }

    public getMenus() : Observable<any> {
        const token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjY2MDM3MzJFMzIyN0MwN0JFOEQwQUJENDcyRjM4NTBBOEYzNDRFRkQiLCJ0eXAiOiJKV1QiLCJ4NWMiOlsibXNzZWd1cmlkYWQiLCJkZW1vLXB2bGciXX0.eyJuYmYiOjE2NDA2NTkyNzAsImV4cCI6MTY0MzI1MTAzMCwiaXNzIjoibXNzZWd1cmlkYWQiLCJhdWQiOiJjMWQ2NDA1MGE0YzI0ZmIxYWFjN2MxM2NmNjIwNTAyNiIsInNjb3BlcyI6eyJJZFVzdWFyaW8iOjQwMjEyLCJVc3VhcmlvIjoiZGVtby1wdmxnIiwiVGlwb1Rva2VuIjoxLCJJZEVudGlkYWRQZXJzb25hIjoxLCJJZEVudGlkYWRFbXByZXNhIjo2MDAxNiwiRG9jdW1lbnRvRW1wcmVzYSI6IjAwMDAwMDAwMDAwIiwiRG9jdW1lbnRvUGVyc29uYSI6IjQzMDk0NTM2IiwiU3VjdXJzYWxlcyI6WzIwLDIxXSwiU2VydmljaW9zIjpbMSwyLDMsNCw1LDYsNyw4LDksMTAsMTEsMTIsMTMsMTQsMTUsMTYsMTcsMTgsMTksMjAsMjEsMjIsMjMsMjQsMjUsMjYsMjcsMjgsMjksMzAsMzEsMzIsMzMsMzQsMzUsMzYsMzcsMzgsMzksNDAsNDEsNDIsNDMsNDQsNDUsNDYsNDcsNDgsNDksNTAsNTEsNTIsNTMsNTQsNTUsNTYsNTcsNTgsNTksNjAsNjEsNjIsNjMsNjQsNjUsNjYsNjcsNjgsNjksNzAsNzEsNzIsNzMsNzQsNzUsNzYsNzcsNzgsNzksODAsODEsODIsODMsODUsODYsODcsODgsODksOTAsOTEsOTIsOTMsOTQsOTUsOTYsOTcsOTgsOTksMTAwLDEwMSwxMDIsMTAzLDEwNCwxMDUsMTA2LDEwNywxMDgsMTA5LDExMCwxMTEsMTEyLDExMywxMTQsMTE1LDExNiwxMTcsMTE4LDExOSwxMjAsMTIxLDEyMiwxMjMsMTI0LDEyNSwxMjYsMTI3LDEyOCwxMjksMTMwLDEzMSwxMzIsMTMzLDEzNCwxMzUsMTM2LDEzNywxMzgsMTM5LDE0MCwxNDEsMTQyLDE0MywxNDQsMTQ1LDE0NiwxNDcsMTQ4LDE0OSwxNTAsMTUxLDE1MiwxNTMsMTU0LDE1NSwxNTYsMTU3LDE1OCwxNjAsMTY1LDE3NywxNzgsMTc5LDE4MSwyMTcsMjE4LDIxOSwyMjIsMjIzLDIzMiwyNzQsMjgxLDI4OCwyODksMzYyLDM3MiwzODEsMzg1LDM4NiwzODcsMzg4LDM4OSwzOTAsMzkxLDM5MiwzOTMsMzk0LDM5NSw0NTEsNDY3LDQ2OCw0NjksNDcwLDUwMCw1MDEsNTAyLDUwMyw1MDQsNTA1LDUwNiw1MDcsNTA4LDUwOSw1MTAsNTExLDUxMiw1MTMsNTE0LDUxNSw1MTYsNTE3LDE3NiwxODAsMTg1LDE2MSwxOTEsMTkyLDIzMywyMzQsMTY2LDE2NywxNjgsMTY5LDE3MCwxNzEsMTcyLDE3MywxNzQsMTc1LDE4MywyMDksMTU5LDE2MiwxNjMsMTY0LDE4NCwyMjcsMjc1LDI3NiwzNTksMzYwLDM2MSwzNzUsMzc2LDg0LDE4MiwxODYsMTg3LDE4OCwxODksMTkwLDE5MywxOTQsMTk1LDE5NiwxOTcsMTk4LDE5OSwyMDAsMjAxLDIwMiwyMDMsMjA0LDIwNSwyMDYsMjA3LDIwOCwyMTAsMjExLDIxMiwyMTMsMjE0LDIxNSwyMTYsMjIwLDIyMSwyMjQsMjI1LDIyNiwyMjgsMjI5LDIzMCwyMzEsMjM1LDIzNiwyMzcsMjM4LDIzOSwyNDAsMjQxLDI0MiwyNDMsMjQ0LDI0NSwyNDYsMjQ3LDI0OCwyNDksMjUwLDI1MSwyNTIsMjUzLDI1NCwyNTUsMjU2LDI1NywyNTgsMjU5LDI2MCwyNjEsMjYyLDI2MywzMjcsMzI4LDI2NCwyNjUsMjY2LDI2NywyNjgsMjY5LDI3MCwyNzEsMjcyLDM1NywzNTgsMjczLDI4NCwyODUsMjg2LDMxOSwzMjAsMzIxLDMyMiwzMjMsMzI0LDMyNSwzMjYsMzI5LDMzMCwzMTcsMzE4LDI3NywyOTAsMzEwLDMxMSwzMTIsMzE1LDMxNiwzMzksMzQzLDM0NCwzNDYsMzQ3LDM1MywzNzcsNDIzLDQyNCw0MjksNDMwLDQzMiw0MzMsNDM0LDQzNSw0MzYsNDQ2LDQ0Nyw0NDgsNDQ5LDQ1MCw0MTksNDIwLDQyMSw0MjIsNDI1LDQyNiw0MjcsNDI4LDQzMSw0MzcsNDM4LDQzOSw0NDAsNDQxLDQ0Miw0NDMsNDQ0LDQ0NSwzOTgsMzk5LDQ1Miw0NTMsNDU0LDQ1NSw0NTYsNDU3LDQ1OCw0NTksNDYwLDQ2MV19fQ.cC2nlJsAX_7xon2atRsKxh3Ek3KChwvBDnwlTCifwcjtHc6PMWs_sXbsSKt6wyuPk9-TxFIP4lMIweyo7ZkyfD3LBTN8pjkDjQh6rnCP8fXh6E_08nC2erZi2yzzz30yRqq6_NA_nCLJltr26R573r6uVtgns2s38rb35bSWKUfVkFGaa-epDivsiFUoQLpmsP0x7NSqSr9n20y8OpBhgvJmfzqoYS9GpIGyDHxa6dtZeRgU2NpymwwnQhiZmbRR2EzYIP7BsS30nerD8ExXathtlBbDyFqTChUg1s-p9g4CYxQ-gayJcu1MNRPh6AktLXQ7PLd5rlDRVGx-clqrpQ';
        const backEnd = 'http://desarrollo.sreasons.com';
        const idUser = 40212;

        const headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${token}`);

        return this._http
          .get(`${backEnd}/MSSeguridad/api/v1/usuarios/${idUser}/permisos/modulos`, { headers: headers })
          .pipe(tap(this.validate), map(res => res))
    }

    private createNuevoMenu(_menu : SeguradMenu[]): MenuModel[]{
        let menuiterable: SeguradMenu[] = _menu || [];
    
        let elMenu: MenuModel = new MenuModel();
        elMenu.id = 0,
        elMenu.label = 'Menú Smart Reasons'
        //elMenu.main = [];
        elMenu.main = menuiterable.map(mapite =>{
          let obj: MainMenuItems = new MainMenuItems()
          obj.id = mapite.Id;
          obj.state = mapite.Ruta;
          obj.name = mapite.Descripcion || '';
          obj.icon = mapite.IconMenu || '';
          obj.short_label = (mapite.Descripcion || 'A').charAt(0);
          // obj.main_state = mapite.Id;
          // obj.target = mapite.Id;
          // obj.badge = mapite.Id;
          if(mapite.Hijos.length == 0){
            obj.type = 'link'
          }
          else{
            obj.type = 'sub';
          }
          mapite.Hijos = mapite.Hijos || []
          obj.children = mapite.Hijos.map(hijosmap =>{
            let objhijo: ChildrenItems =  new ChildrenItems();
            objhijo.id = hijosmap.Id;
            objhijo.state = hijosmap.Ruta;
            // objhijo.target = hijosmap.Id;
            objhijo.name = hijosmap.Descripcion || '';
            objhijo.icon = hijosmap.IconMenu || '';
            if(hijosmap.Hijos.length != 0){
              objhijo.type = 'sub'
            }
    
            objhijo.children = hijosmap.Hijos.map(hijosnexmap =>{
              let objhijonew: ChildrenItems =  new ChildrenItems();
              objhijonew.id = hijosnexmap.Id;
              objhijonew.state = hijosnexmap.Ruta;
              // objhijo.target = hijosmap.Id;
              objhijonew.name = hijosnexmap.Descripcion || '';
              objhijonew.icon = hijosnexmap.IconMenu || '';
              if(hijosmap.SubMenu == null){
                objhijonew.type = 'admin'
              }
              objhijonew.children = [];
              return objhijonew;
            });
            return objhijo;
          });
    
          
          return obj;
        });
    
        return  [elMenu];
      }

    private handleError(error: Response){
        let mensaje = `Error status code ${error.status} en ${error.url}`
        return Observable.throw(mensaje);
    }

    validate(res) {
        if (res && res.body && +res.body.codigo !== 200) {
          if (res.body && res.body.mensaje) {
            throw new Error(res.body.mensaje);
          } else {
            throw new Error('El servicio no ha respondido correctamente');
          }
        }
      }
}