export class MenuModel {
  id: number;
  label: string;
  main?: MainMenuItems[] = [];
}

export class MainMenuItems {
  id: number;
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export class BadgeItem {
  type: string;
  value: string;
}

export class ChildrenItems {
  id: number;
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  icon?: string;
  children?: ChildrenItems[];
}

export class Generico<T> {
  Id: T;
  LogUsuariocrea: string;
  LogFechacrea: string;
  LogUsuariomodif: string;
  LogFechamodif: string;
  LogEstado: number;

  constructor() {}
}

export class SeguradMenu extends Generico<number> {
  IdPermiso?: number;
  IdParent?: number;
  Tipo: number;
  IconMenu: string;
  Descripcion?: string;
  Nivel?: number;
  Ruta: string;
  Hijos: Hijos[] = [];
  SubMenu?: string;
  constructor() {
    super();
  }
}
export class Hijos extends Generico<number> {
  IdPermiso?: number;
  IdParent?: number;
  Tipo: number;
  IconMenu: string;
  Descripcion?: string;
  Nivel?: number;
  Ruta: string;
  SubMenu?: string;
  Hijos?: Hijos[] = [];
}
